
const express = require('express')
const app = express()

app.use(express.static(__dirname));

app.listen(8080, () => {
  console.log('server is listening')
})

app.get('/', (req, res) => {
  res.render('index')
})
