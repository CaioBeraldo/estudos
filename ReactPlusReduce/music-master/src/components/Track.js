import React from 'react'

const Track = ({ track, playAudio, trackIcon }) => {
  const { album } = track
  const albumImage = album.images[0] || { url: '' }


  return (
    <div className="artist-track" onClick={playAudio(track.preview_url)}>
      <img src={albumImage.url} className="track-album"></img>
      <span className="track-text">
        {track.name}
      </span>
      <p className="track-icon">{trackIcon(track)}</p>
    </div>
  )
}

export default Track