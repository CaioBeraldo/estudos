import React, { useState } from 'react'

const Search = ({ searchArtist }) => {
  const [artistQuery, setArtistQuery] = useState('')

  const hangleKeyPress = (event) => {
    if (event.key === 'Enter') {
      searchArtist(artistQuery)
    }
  }

  return(
    <div>
      <input
        className="input"
        placeholder="Search for an artist..." 
        onChange={(event) => setArtistQuery(event.target.value)}
        onKeyPress={hangleKeyPress}
        />

      <button
        className="button"
        onClick={() => searchArtist(artistQuery)}>
        Search
      </button>
    </div>
  )
}

export default Search