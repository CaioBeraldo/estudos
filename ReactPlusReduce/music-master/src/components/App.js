import React, { useState } from 'react'
import { getArtist, getTopTracks } from '/apis/spotify-api'
import Artist from './Artist';
import TopTracks from './TopTracks'
import Search from './Search'

const App = () => {
  const [artist, setArtist] = useState(null)
  const [topTracks, setTopTracks] = useState(null)

  const searchArtist = (artistQuery) => {
    setArtist(null)
    setTopTracks(null)

    getArtist(artistQuery)
      .then(artist => setArtist(artist) || getTopTracks(artist))
      .then(setTopTracks)
      .catch(error => alert(error.message))
  }

  return (
    <div>
      <h2>Music Master</h2>

      <Search searchArtist={searchArtist} />

      <Artist artist={artist} />

      <TopTracks topTracks={topTracks} />
    </div>
  )
}

export default App
