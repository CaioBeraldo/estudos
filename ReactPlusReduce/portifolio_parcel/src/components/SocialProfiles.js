import React from 'react';
import SOCIAL_PROFILES from '../data/socialProfiles';

const SocialProfiles = () =>  (
  <div>
    <h2>Connect with me!</h2>
    {SOCIAL_PROFILES.map(socialProfile => (
      <SocialProfile key={socialProfile.id} socialProfile={socialProfile} />
    ))}
  </div>
)

const SocialProfile = (props) => {
  const { link, image } = props.socialProfile

  return (
    <div style={{ display: 'inline-block', margin: 10 }}>
      <a href={link}>
        <img src={image} alt="social profile" style={{ width: 35, height: 35 }} />
      </a>
    </div>
  )
}

export default SocialProfiles