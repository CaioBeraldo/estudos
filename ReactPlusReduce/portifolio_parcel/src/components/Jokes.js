import React, { Component } from 'react'

const Joke = ({ joke: { setup, punchline } }) => (
  <p>{setup} <em>{punchline}</em></p>
)

class Jokes extends Component {
  state = { joke: {}, jokes: [] }

  componentDidMount() {
    this.fetchAJoke()
  }

  fetchAJoke = () => {
    fetch('https://official-joke-api.appspot.com/random_joke')
      .then(data => data.json())
      .then(joke => this.setState({ joke }))
      .catch(error => console.warn(error))
  }

  fetchTenMoreJokes = () => {
    fetch('https://official-joke-api.appspot.com/random_ten')
      .then(data => data.json())
      .then(jokes => this.setState({ jokes }))
      .catch(error => console.warn(error))
  }

  render() {
    return (
      <div>
        <h2>Highligted Jokes</h2>
        <Joke joke={this.state.joke} />

        <button onClick={this.fetchTenMoreJokes}>See more Jokes</button>
        {this.state.jokes.map(joke => (
          <Joke key={joke.id} joke={joke} />
        ))}
      </div>
    )
  }
}

export default Jokes