import React, { useState } from 'react'
import Track from './Track'

const TopTracks = ({ topTracks }) => {
  if (!topTracks) {
    return null
  }

  const { tracks } = topTracks

  const [playing, setPlaying] = useState(false)
  const [audio, setAudio] = useState(null)
  const [playingPreview, setPlayingPreview] = useState(null)

  const playAudio = (previewUrl) => () => {
    const audio_preview = new Audio(previewUrl)
    
    if (!playing) {
      audio_preview.play()
      setPlaying(true)
      setPlayingPreview(previewUrl)
      setAudio(audio_preview)
    }
    else {
      audio.pause()

      if (playingPreview !== previewUrl) {
        audio_preview.play()
        setPlaying(true)
        setPlayingPreview(previewUrl)
        setAudio(audio_preview)
        }
      else {
        setAudio(null)
        setPlayingPreview(null)
        setPlaying(false)
      }
    }
  }

  const trackIcon = (track) => {
    if (!track.preview_url) {
      // not available
      return <span>N/A</span>
    }

    if (playing && playingPreview === track.preview_url) {
      return <span>| |</span> 
    }
    else {
      return <span>&#9654;</span> 
    }
  }

  return (
    <div>
      <h2>Top Tracks</h2>
      <div className="artist-tracks">
        {tracks.map(track => (
          <Track 
            key={track.id}
            track={track}
            playAudio={playAudio} 
            trackIcon={trackIcon}
            />
        ))}
      </div>
    </div>
  )
}

export default TopTracks