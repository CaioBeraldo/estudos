import React from 'react'

const Artist = ({ artist }) => {
  if (!artist) return null

  const { images, name, followers, popularity, genres } = artist

  const image = images[0] || { url: '' }

  return (
    <div className="artist-card">
      <img src={image.url} alt="imagem do artista" className="artist-picture"></img>

      <h1>{name}</h1>

      <div>
        <span>
          {followers.total}
        </span>
        <span>
          {popularity}
        </span>
      </div>

      <span>{genres.join(', ')}</span>
    </div>
  )
}

export default Artist