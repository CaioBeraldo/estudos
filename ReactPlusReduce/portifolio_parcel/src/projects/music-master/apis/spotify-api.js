
const API_ADDRESS = 'https://spotify-api-wrapper.appspot.com'

export const getArtist = (artist) => {
  return new Promise((resolve, reject) => {
    if (!artist) {
      resolve()
      return
    }

    fetch(`${API_ADDRESS}/artist/${artist}`)
      .then(response => response.json())
      .then((data) => data.artists.items.map(artist => ({ ...artist })).shift())
      .then(resolve)
      .catch(reject)
  })
}

export const getTopTracks = (artist) => {
  return new Promise((resolve, reject) => {
    if (!artist) {
      resolve()
      return
    }

    fetch(`${API_ADDRESS}/artist/${artist.id}/top-tracks`)
      .then(response => response.json())
      .then(resolve)
      .catch(reject)
  })
}
