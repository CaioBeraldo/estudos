import React from 'react'
import { connect } from 'react-redux'
import NewGame from './Game/NewGame'
import StopGame from './Game/StopGame'
import Instructions from './Game/Instructions'
import Loading from './Game/Loading'

const App = ({ gameStarted, gameStarting }) => (
  <div>
    <h2>Evens or Odds</h2>

    {gameStarted ? <StopGame /> : gameStarting ? <Loading /> : <NewGame /> }

    <Instructions />
  </div>
)

const mapStateToProps = state => ({
  gameStarted: state.gameStarted,
  gameStarting: state.gameStarting,
})

export default connect(mapStateToProps)(App)
