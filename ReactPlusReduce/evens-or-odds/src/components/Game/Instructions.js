import React from 'react'
import { connect } from 'react-redux'
import { expandInstructions, collapseInstructions } from '../../store/actions'

const Instructions = ({ instructionsExpanded, expandInstructions, collapseInstructions }) => (
  !instructionsExpanded ?
    <div>
      <h3>Instructions</h3>
      <p>Welcome to evens or odds. The game looks like this...</p>
      <button onClick={expandInstructions}>Read more</button>
    </div>
    :
    <div>
      <h3>Instructions</h3>
      <p>Welcome to evens or odds. The game looks like this...</p>
      <p>The deck is shuffled. Then choose: will the next card be even or odd?</p>
      <p>Make a choice on every draw, and see how many you get right!</p>
      <p>(Face cards don't count)</p>

      <button onClick={collapseInstructions}>Show less</button>
    </div>
)

const mapStateToProps = (state) => ({ instructionsExpanded: state.instructionsExpanded })

const mapDispatchToProps = ({ expandInstructions, collapseInstructions })

export default connect(mapStateToProps, mapDispatchToProps)(Instructions)
