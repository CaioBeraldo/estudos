import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { cancelGame } from '../../store/actions'

const StopGame = ({ cancelGame }) => (
  <div>
    <h3>The game is on!</h3>
    <button onClick={cancelGame}>Cancel</button>
  </div>
)

const mapDispatchToProps = (dispatch) => bindActionCreators({ cancelGame }, dispatch)

export default connect(null, mapDispatchToProps)(StopGame)
