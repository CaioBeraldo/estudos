import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchNewDeck } from '../../store/actions/deck'

const NewGame = ({ fetchNewDeck }) => (
  <div>
    <h3>A new game awaits...</h3> 
    <button onClick={fetchNewDeck}>Start</button>
  </div>
)

const mapDispatchToProps = (dispatch) => bindActionCreators({ fetchNewDeck }, dispatch)

export default connect(null, mapDispatchToProps)(NewGame)
