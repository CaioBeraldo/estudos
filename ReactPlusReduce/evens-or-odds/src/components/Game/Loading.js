import React from 'react'

const Loading = () => (
  <h3>Aguarde, carregando...</h3>
)

export default Loading
