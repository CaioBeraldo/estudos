import { DECK } from './types'
import { startingGame, startGame } from './index'

export const fetchDeckSuccess = ({ remaining, deck_id }) => ({
  type: DECK.FETCH_SUCCESS,
  remaining,
  deck_id,
})

export const fetchDeckError = ({ message }) => ({
  type: DECK.FETCH_ERROR,
  message
})

const handleResponse = (response) => {
  if (response.status !== 200) {
    throw new Error('Unsuccessful request to deckofcardsapi.com')
  }

  return response.json()
}

export const fetchNewDeck = () => (dispatch) => {
  dispatch(startingGame(true))
  return fetch('https://deck-of-cards-api-wrapper.appspot.com/deck/new/shuffle')
    .then(handleResponse)
    .then(deck => dispatch(fetchDeckSuccess(deck)))
    .then(() => dispatch(startGame()))
    .catch(error => dispatch(fetchDeckError(error)))
    .finally(() => dispatch(startingGame(false)))
}
