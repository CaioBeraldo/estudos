import { SET_GAME_STARTED, SET_INSTRUCTIONS_EXPANDED, SET_GAME_STARTING } from '../actions/types'
import { DECK } from '../actions/types';

const DEFAULT_SETTINGS = {
  gameStarted: false,
  gameStarting: false,
  instructionsExpanded: false,
  deck_id: '',
  remaining: 0,
  error_message: ''
}

const rootReducer = (state = DEFAULT_SETTINGS, action) => {
  switch (action.type) {
    case SET_GAME_STARTED:
      return { ...state, gameStarted: action.gameStarted }
    case SET_GAME_STARTING:
      return { ...state, gameStarting: action.gameStarting }
    case SET_INSTRUCTIONS_EXPANDED:
      return { ...state, instructionsExpanded: action.instructionsExpanded }
    case DECK.FETCH_SUCCESS:
      return { ...state, deck_id: action.deck_id, remaining: action.remaining }
    case DECK.FETCH_ERROR:
      return { ...state, error_message: action.message }
    default:
      return state
  }
}

export default rootReducer
