import React from 'react';
import ReactDOM from 'react-dom';

const div = React.createElement('div', null, <div>React Element with JSX!</div>)

ReactDOM.render(div, document.getElementById('root'))
